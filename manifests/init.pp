class mongodb-10gen {

   file { '/etc/apt/trusted.gpg.d/mongodb-10gen.gpg':
     ensure => file,
     owner => 'root',
     group => 'root', 
     mode => 0640,
     source => 'puppet:///modules/mongodb-10gen/etc/apt/trusted.gpg.d/mongodb-10gen.gpg',
   }

   file { '/etc/apt/sources.list.d/mongodb-10gen.list':
     ensure => file,
     owner => 'root',
     group => 'root', 
     mode => 0640,
     source => 'puppet:///modules/mongodb-10gen/etc/apt/sources.list.d/mongodb-10gen.list',
     require => File['/etc/apt/trusted.gpg.d/mongodb-10gen.gpg'],
     notify => Exec['apt-update'],
   }

   package { [
               'mongodb-10gen',
             ]:
     ensure => latest, 
     require => Exec['apt-update'],    
  }

  File {
     owner => 'mongodb',
     group => 'mongodb',
     mode  => 0755,
  }

  file { [
           '/home/mongodb',
           '/home/mongodb/bin'
         ]: 
     ensure => directory,
     require => Package['mongodb-10gen'],
  }

  file { [
           '/home/mongodb/bin/automongobackup.sh'
         ]:
     ensure => file,
     source => 'puppet:///modules/mongodb-10gen/home/mongodb/bin/automongobackup/src/automongobackup.sh',
     require => File['/home/mongodb/bin'],
  }

}
